package com.djamware.myspringbootapp.controller;

import com.djamware.myspringbootapp.entity.MyProduct;
import com.djamware.myspringbootapp.repository.MyProductRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/product")
public class MyProductController {

    private final MyProductRepository myProductRepository;

    public MyProductController(MyProductRepository myProductRepository) {
        this.myProductRepository = myProductRepository;
    }

    @GetMapping()
    public @ResponseBody
    Iterable<MyProduct> getAll() {
        return myProductRepository.findAll();
    }

}
